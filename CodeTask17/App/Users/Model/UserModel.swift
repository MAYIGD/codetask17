//
//  UserModel.swift
//  CodeTask17
//
//  Created by GDGD on 2021/2/28.
//

import Foundation

struct User: Codable, Equatable {
    let id: Int
    let avatar_url: String
    let login: String
}

struct UserQuery: Equatable {
    let query: String
}

struct UsersPage: Equatable {
    let page: Int
    let totalPages: Int
    let users: [User]
}
