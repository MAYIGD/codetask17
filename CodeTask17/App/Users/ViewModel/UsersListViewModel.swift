//
//  UsersListViewModel.swift
//  CodeTask17
//
//  Created by GDGD on 2021/2/28.
//

import Foundation

enum UsersListViewModelLoading {
    case fullScreen
    case nextPage
}

protocol UsersListViewModelInput {
    func viewDidLoad()
    func didLoadNextPage()
    func didSearch(query: String)
    func didCancelSearch()
//    func didSelectItem(at index: Int)
}

protocol UsersListViewModelOutput {
    var items: Observable<[User]> { get }
    var loading: Observable<UsersListViewModelLoading?> { get }
    var query: Observable<String> { get }
    var error: Observable<String> { get }
    var isEmpty: Bool { get }
}

protocol UsersListViewModel: UsersListViewModelInput, UsersListViewModelOutput {}

final class DefaultUsersListViewModel: UsersListViewModel {

    var per_page: Int = 100
    var currentPage: Int = 0
    var totalPageCount: Int = 1
    var hasMorePages: Bool { currentPage < totalPageCount }
    var nextPage: Int { hasMorePages ? currentPage + 1 : currentPage }

    private var pages: [UsersPage] = []
    
    let items: Observable<[User]> = Observable([])
    let loading: Observable<UsersListViewModelLoading?> = Observable(.none)
    let query: Observable<String> = Observable("")
    let error: Observable<String> = Observable("")
    var isEmpty: Bool { return items.value.isEmpty }

    init(){

    }

    private func appendPage(_ usersPage: UsersPage) {
        pages = pages
            .filter { $0.page != usersPage.page }
            + [usersPage]

        items.value = pages.users
    }

    private func resetPages() {
        currentPage = 0
        totalPageCount = 1
        pages.removeAll()
        items.value.removeAll()
    }

    private func load(userQuery: UserQuery, loading: UsersListViewModelLoading) {
        self.loading.value = loading
        query.value = userQuery.query
        
        let parameters = [
            "q":query.value,
            "per_page":per_page,
            "page":nextPage
            ] as [String : Any]
        
        QueryService().getTask(url: "https://api.github.com/search/users", parameters: parameters) { (ok, obj) in
            
            if ok{
                if let json = obj as? [String:Any]{
                    if let total = json["total_count"] as? Int,
                       let ary = json["items"] as? [[String: Any]] {
                        let data = ary.makeModel(User.self)

                        self.totalPageCount = total / 100 + 1;
                        self.currentPage += 1
                        let page = UsersPage(page: self.currentPage, totalPages: self.totalPageCount, users: data)
                        self.appendPage(page)
                        self.loading.value = .none
                    }
                }
            }
        }

    }

    private func update(userQuery: UserQuery) {
        resetPages()
        load(userQuery: userQuery, loading: .fullScreen)
    }

    // MARK: - UsersListViewModelInput
    func viewDidLoad() { }

    func didLoadNextPage() {
        guard hasMorePages, loading.value == .none else { return }
        load(userQuery: .init(query: query.value),
             loading: .nextPage)
    }

    func didSearch(query: String) {
        guard !query.isEmpty else { return }
        update(userQuery: UserQuery(query: query))
    }

    func didCancelSearch() {
    }

}

// MARK: - Private

private extension Array where Element == UsersPage {
    var users: [User] { flatMap { $0.users } }
}

