//
//  UsersViewController.swift
//  CodeTask17
//
//  Created by GDGD on 2021/2/28.
//

import UIKit

class UsersViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var searchBarContainer: UIView!
    private var searchController = UISearchController(searchResultsController: nil)
    
    var viewModel: UsersListViewModel!
    var nextPageLoadingSpinner: UIActivityIndicatorView?
    
    static func create(viewModel: UsersListViewModel) -> UsersViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "UsersViewController") as! UsersViewController

        viewController.viewModel = viewModel
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Users"
        setupSearchController()
        setupList()
        bind()
        
    }
    
    private func setupSearchController() {
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search Users"
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.translatesAutoresizingMaskIntoConstraints = true
        searchController.searchBar.barStyle = .default
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.frame = searchBarContainer.bounds
        searchController.searchBar.autoresizingMask = [.flexibleWidth]
        searchBarContainer.addSubview(searchController.searchBar)
        definesPresentationContext = true
    }
    
    private func setupList() {
        
        let contantPadding :CGFloat = 12
        let itemSpacing :CGFloat = 12
        
        let layout = UICollectionViewFullWidthFlowLayout()
         layout.minimumLineSpacing = itemSpacing
        layout.sectionInset = UIEdgeInsets(top: contantPadding, left: contantPadding, bottom: contantPadding, right: contantPadding)
        collectionView.collectionViewLayout = layout
        collectionView.register(UINib(nibName: UserCell.className, bundle: nil), forCellWithReuseIdentifier: UserCell.identifiter)
        collectionView.register(CollectionViewFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "Footer")
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.footerReferenceSize = CGSize(width: collectionView.bounds.width, height: 50)
        collectionView.alwaysBounceVertical = true
        collectionView.keyboardDismissMode = .interactive
        collectionView.isPrefetchingEnabled = false
        collectionView.contentInsetAdjustmentBehavior = .always
        //collectionView.automaticallyAdjustsScrollIndicatorInsets = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    private func bind() {
        viewModel.items.observe(on: self) { [weak self] _ in self?.updateItems() }
        viewModel.loading.observe(on: self) { [weak self] in self?.updateLoading($0) }
        viewModel.query.observe(on: self) { [weak self] in self?.updateSearchQuery($0) }
    }
    
    private func updateItems() {
        collectionView.reloadData()
    }
    
    private func updateSearchQuery(_ query: String) {
        searchController.isActive = false
        searchController.searchBar.text = query
    }
    
    private func updateLoading(_ loading: UsersListViewModelLoading?) {
        collectionView.isHidden = true
        LoadingView.hide()

        switch loading {
        case .fullScreen: LoadingView.show()
        case .nextPage: collectionView.isHidden = false
        case .none:
            collectionView.isHidden = viewModel.isEmpty
        }
    }
    
    private func makeActivityIndicator(size: CGSize) -> UIActivityIndicatorView {
        let style: UIActivityIndicatorView.Style
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                style = .white
            } else {
                style = .gray
            }
        } else {
            style = .gray
        }

        let activityIndicator = UIActivityIndicatorView(style: style)
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        activityIndicator.frame = .init(origin: .zero, size: size)

        return activityIndicator
    }

}

extension UsersViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text, !searchText.isEmpty else { return }
        searchController.isActive = false
        viewModel.didSearch(query: searchText)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.didCancelSearch()
    }
}

extension UsersViewController: UISearchControllerDelegate {
    public func willPresentSearchController(_ searchController: UISearchController) {
        
    }

    public func willDismissSearchController(_ searchController: UISearchController) {

    }

    public func didDismissSearchController(_ searchController: UISearchController) {

    }
}

extension UsersViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.items.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        switch viewModel.loading.value {
        case .nextPage:
            return CGSize(width: collectionView.frame.width, height: 44)
            
        case .fullScreen, .none:
            return CGSize.zero
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)
            
            nextPageLoadingSpinner?.removeFromSuperview()
            
            switch viewModel.loading.value {
            case .nextPage:
                nextPageLoadingSpinner = makeActivityIndicator(size: .init(width: collectionView.bounds.width, height: 44))
                footer.addSubview(nextPageLoadingSpinner!)
                
            case .fullScreen, .none:
                break
            }

            return footer
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UserCell.identifiter, for: indexPath) as! UserCell
        
        cell.fill(model: viewModel.items.value[indexPath.row])
        
        if indexPath.row == viewModel.items.value.count - 1 {
            viewModel.didLoadNextPage()
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let fullWidthLayout=collectionViewLayout as! UICollectionViewFullWidthFlowLayout
        return CGSize(width: fullWidthLayout.fullWidth(forBounds: collectionView.bounds), height:60)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    
}

public class CollectionViewFooterView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

