//
//  UserCell.swift
//  CodeTask17
//
//  Created by GDGD on 2021/2/28.
//

import UIKit
import Kingfisher

class UserCell: UICollectionViewCell {
    static let identifiter = "UserCell"
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    private var model: User!
    
    func fill(model: User) {
        self.model = model
        idLabel.text = "id: \(model.id)"
        nameLabel.text = "name: \(model.login)"
        imgView.image = nil
        loadImage(model.avatar_url)
    }
    
    private func loadImage(_ urlStr: String) {
        
        guard let url = URL(string: urlStr) else { return }
        
        KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                //print("Image: \(value.image). Got from: \(value.cacheType)")
                self.imgView.image = value.image
            case .failure(let error):
                print("KingfisherManager retrieveImage Error: \(error)")
            }
        }
    }
}
