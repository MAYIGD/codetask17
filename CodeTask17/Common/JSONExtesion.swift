//
//  JSONExtesion.swift
//  CodeTask17
//
//  Created by GDGD on 2021/2/28.
//

import Foundation
extension JSONDecoder {
    func decode<T: Decodable>(_ type: T.Type, withJSONObject object: Any, options opt: JSONSerialization.WritingOptions = []) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: object, options: opt)
        return try decode(T.self, from: data)
    }
}

extension Dictionary where Key == String, Value:Any {
    
    func makeModel<T: Decodable>(_ type: T.Type) -> T? {
        
        do {
            let jsonDecoder = JSONDecoder()
            let model = try jsonDecoder.decode(T.self, withJSONObject: self)
            return model
            
        }catch let DecodingError.dataCorrupted(context) {
            print(context)
            return nil
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return nil
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return nil
        } catch let DecodingError.typeMismatch(type, context)  {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return nil
        } catch {
            print(error.localizedDescription)
            return nil
        }
        
    }
}

extension Array where Element == Dictionary<String, Any> {
    
    func makeModel<T: Decodable>(_ type: T.Type) -> [T] {
        
        do {
            let jsonDecoder = JSONDecoder()
            let model = try jsonDecoder.decode([T].self, withJSONObject: self)
            return model
            
        }catch let DecodingError.dataCorrupted(context) {
            print(context)
            return []
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return []
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return []
        } catch let DecodingError.typeMismatch(type, context)  {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
            return []
        } catch {
            print(error.localizedDescription)
            return []
        }
        
    }
}
