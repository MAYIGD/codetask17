//
//  QueryService.swift
//  CodeTask17
//
//  Created by GDGD on 2021/2/28.
//

import UIKit

class QueryService {
    
    private func getRequest(url:String) -> URLRequest{
        
        let urlString = URL(string: url)!
        var request = URLRequest(url: urlString)
        request.setValue("application/json", forHTTPHeaderField: "content")
        request.setValue("no-cache", forHTTPHeaderField: "cache-control")
        
        return request
    }
    
    public func getTask(url:String, completion: @escaping ((Bool, Any?) -> Void)){
        
        debugPrint(url)
        
//        if !ReachabilityHandler.shared.isNetworkAvailable{
//            completion(false, Constants.errorNetwork)
//            print(Constants.errorNetwork)
//            return
//        }
        
        let request = getRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { (ok, obj) in
            completion(ok, obj)
        }
        task.resume()
    }
    
    public func getTask(url:String, parameters:[String : Any], completion: @escaping ((Bool, Any?) -> Void)){
        
        let _url = url+"?"+parameters.percentEscaped();
        debugPrint(_url)
        
//        if !ReachabilityHandler.shared.isNetworkAvailable{
//            completion(false, Constants.errorNetwork)
//            print(Constants.errorNetwork)
//            return
//        }
        

        let request = getRequest(url: _url)
        let task = URLSession.shared.dataTask(with: request) { (ok, obj) in
            completion(ok, obj)
        }
        task.resume()
    }
    
}

extension URLSession {
    
    
    func dataTask(with request: URLRequest, completion: @escaping ((Bool, Any?) -> Void)) -> URLSessionDataTask {
        return dataTask(with: request, completionHandler: { (data, response, error) in
            
//            debugPrint(request.url!)
            
            guard error == nil else {
                completion(false, "伺服器異常")
                return
            }
            
//            print((response as! HTTPURLResponse).statusCode)
            
            guard
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 else {
                    completion(false, 401)
                    return
            }
            
            // make sure we got data
            guard let responseData = data else {
                completion(false, "資料異常")
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any] {
//                    debugPrint(json)
   
                    completion(true, json)
             
                }
            } catch let error {
                print(error.localizedDescription)
                completion(false, error.localizedDescription)
            }
        })
    }
}

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
